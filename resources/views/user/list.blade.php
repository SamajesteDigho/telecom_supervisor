@extends('layouts.app')

@section('content')
    <div class="card p-3">
        <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
            <div class="card-body pt-0">
                <div class="table-responsive">
                    <table id="example3" class="display table" style="min-width: 100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Username</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone</th>
                                <th>Role</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>
                                        <img class="rounded-circle" width="35"
                                            src="{{ asset('assets/data/images/profile/avatar.jpg') }}" alt="">
                                    </td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->firstname }}</td>
                                    <td>{{ $user->lastname }}</td>
                                    <td><a href="javascript:void(0);"><strong>{{ $user->phone }}</strong></a></td>
                                    <td>
                                        @if ($user->role == 'ADMIN')
                                            <a href="javascript:void(0);" class="badge badge-rounded badge-success">
                                                {{ $user->role }}
                                            </a>
                                        @else
                                            <a href="javascript:void(0);" class="badge badge-rounded badge-primary">
                                                {{ $user->role }}
                                        @endif
                                    </td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="javascript:void(0);" class="btn btn-primary shadow btn-xs sharp me-1"
                                                data-bs-toggle="modal" data-bs-target=".bd-example-modal-sm">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-danger shadow btn-xs sharp me-1"
                                                data-bs-toggle="modal" data-bs-target=".bd-example-modal-sm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
                                                aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Info Tagging</h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal">
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">This module is still on development. See you
                                                            later for cautions and precautions</div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger light"
                                                                data-bs-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
