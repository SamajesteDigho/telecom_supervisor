@extends('layouts.app')

@section('content')
    <form class="profile-form" action="{{ route('user.account.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-xl-3 col-lg-3">

            </div>

            <div class="col-xl-6 col-lg-6">
                <div class="card profile-card card-bx">
                    <div class="card-header">
                        <h6 class="title">Add User</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 mb-2">
                                <label class="form-label">Username</label>
                                <input type="text" class="form-control @error('username') danger @enderror"
                                    name="username" id="username" value="{{ old('username') }}">
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label class="form-label">Gender</label>
                                <select class="default-select form-control" name="role" id="role">
                                    <option value="USER">User</option>
                                    <option value="ADMIN">Admin</option>
                                </select>
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label class="form-label">First Name</label>
                                <input type="text" class="form-control" value="{{ old('firstname') }}" name="firstname"
                                    id="firstname">
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label class="form-label">Last Name</label>
                                <input type="text" class="form-control" name="lastname" id="lastname"
                                    value="{{ old('lastname') }}">
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" id="email"
                                    value="{{ old('email') }}">
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label>Phone</label>
                                <input type="text" class="form-control" name="phone" id="phone"
                                    value="{{ old('phone') }}">
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label class="form-label">Password</label>
                                <input type="text" class="form-control" name="password" id="password">
                            </div>
                            <div class="col-sm-6 mb-2 p-5">
                                <div class="author-profile">
                                    <div class="author-media">
                                        <img src="{{ asset('assets/data/images/profile/avatar.jpg') }}" alt="">
                                        <div class="upload-link" title="" data-toggle="tooltip" data-placement="right"
                                            data-original-title="update">
                                            <input type="file" class="update-flie" id="avatar" name="avatar">
                                            <i class="fa fa-camera"></i>
                                        </div>
                                    </div>
                                    <div class="author-info">
                                        <span><i>Click on Camera to upload image</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <input type="submit" class="btn btn-primary" value="Validate">
                        <a href="page-register.html" class="btn-link">Forgot your password?</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
