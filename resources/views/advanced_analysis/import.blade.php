@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="nav flex-column nav-pills mb-3">
                    <strong>Select Source :</strong>
                    <a href="#local" data-bs-toggle="pill" class="nav-link active show">Stockage Locale</a>
                    <a href="#remote" data-bs-toggle="pill" class="nav-link">Depuis un serveur FTP</a>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="tab-content">
                    <div id="local" class="tab-pane fade active show">
                        <div class="col-xl-6 col-lg-12">
                            <form action="{{ route('user.advanced_analysis.upload') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Chargez les fichiers à traiter</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <label for="kpis" class="form-label">Fichier des KPI Paramètres</label>
                                            <input class="form-control" type="file" id="kpis" name="kpis"
                                                accept=".csv,.xls,.xlsx" required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label">Choisir la Technologie</label>
                                            <div class="form-check form-check-inline mx-3">
                                                <input type="radio" class="form-check-input" id="2g"
                                                    name="generation" value="2G" checked>
                                                <label class="form-check-label" for="2g">2G</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input type="radio" class="form-check-input" id="3g"
                                                    name="generation" value="3G">
                                                <label class="form-check-label" for="3g">3G</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <input type="submit" class="btn btn-primary" value="Import">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="remote" class="tab-pane fade">
                        <div class="col-xl-12 col-lg-12">
                            <form action="{{ route('user.import.ftp_upload') }}" method="POST">
                                @csrf
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Chargez les fichiers caractéristique (Remote FTP)</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="mb-3 col-md-8">
                                                <label class="form-label">Serveur FTP</label>
                                                <input type="text" class="form-control" name="ftp_server" id="ftp_server"
                                                    placeholder="ftp.example.onet" required>
                                            </div>
                                            <div class="mb-3 col-md-4">
                                                <label class="form-label">Port FTP</label>
                                                <input type="number" class="form-control" name="ftp_port" id="ftp_port"
                                                    placeholder="21">
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Nom d'utilisateur FTP</label>
                                                <input type="text" class="form-control" name="ftp_username"
                                                    id="ftp_username" placeholder="Login" required>
                                            </div>
                                            <div class="mb-3 col-md-6">
                                                <label class="form-label">Mot de passe FTP</label>
                                                <input type="password" class="form-control" name="ftp_password"
                                                    id="ftp_password" placeholder="******">
                                            </div>
                                            <div class="mb-3 col-md-8">
                                                <label class="form-label">Emplacement du fichier dans le serveur
                                                    FTP</label>
                                                <input type="text" class="form-control" name="ftp_server_file"
                                                    id="ftp_server_file" placeholder="/chemin/vers/ficher/a_telecharger"
                                                    required>
                                            </div>
                                            <div class="mb-3 col-md-4">
                                                <label class="form-label">Extension</label>
                                                <input type="text" class="form-control"
                                                    name="ftp_server_file_extension" id="ftp_server_file_extension"
                                                    value="xlsx">
                                            </div>
                                            <div class="mb-3 col-md-3 col-xl-3">
                                                <label class="form-label">Choisir la Technologie</label>
                                                <div class="form-check form-check-inline mx-3">
                                                    <input type="radio" class="form-check-input" id="2g"
                                                        name="generation" value="2G" checked>
                                                    <label class="form-check-label" for="2g">2G</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" class="form-check-input" id="3g"
                                                        name="generation" value="3G">
                                                    <label class="form-check-label" for="3g">3G</label>
                                                </div>
                                            </div>
                                            <div class="mb-3 col-md-4">
                                                <label for="kpis" class="form-label">Date à séléctionner</label>
                                                <input class="form-control" type="date" id="selected_date"
                                                    name="selected_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <input type="submit" class="btn btn-primary" value="Import">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
