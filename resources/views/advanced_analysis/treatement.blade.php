@extends('layouts.app')

@section('content')
    <script>
        var COUVERTURE = @json($couverture);
        var CAPACITE = @json($capacite);
        var QUALITE = @json($qualite);
    </script>
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="default-tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#couverture">
                                Couverture
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#capacite">
                                Capacité
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#qualite">
                                Qualité
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#transport">
                                Transport
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="couverture" role="tabpanel">
                            <div class="pt-4">
                                <div class="card-header flex-wrap d-flex justify-content-between">
                                    <div>
                                        <h4>Couverture</h4>
                                    </div>
                                    <button type="button" class="btn btn-sm btn-primary" onclick="analyser('COUVERTURE')">
                                        Analyser
                                    </button>
                                </div>
                                <div class="row">
                                    <!-- Column starts -->
                                    <div class="col-xl-12">
                                        <div class="card dz-card" id="accordion-one">
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="Preview" role="tabpanel"
                                                    aria-labelledby="home-tab">
                                                    <div class="card-body pt-0">
                                                        <div class="table-responsive">
                                                            <table id="couverture_datatable" class="display table"
                                                                style="min-width: 100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        @foreach ($couverture_header as $item)
                                                                            <th>{{ $item }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($couverture as $item)
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>{{ $item->bcfname }}</td>
                                                                            <td>{{ $item->btsname }}</td>
                                                                            <td>{{ $item->sitecode }}</td>
                                                                            <td>{{ $item->detected }}</td>
                                                                            <td>{{ $item->value7 }}</td>
                                                                            <td>{{ $item->value1 }}</td>
                                                                            <td>{{ $item->value2 }}</td>
                                                                            <td>{{ $item->value3 }}</td>
                                                                            <td>{{ $item->value4 }}</td>
                                                                            <td>{{ $item->value5 }}</td>
                                                                            <td>{{ $item->value6 }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        @foreach ($couverture_header as $item)
                                                                            <th>{{ $item }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="capacite">
                            <div class="pt-4">
                                <div class="card-header flex-wrap d-flex justify-content-between">
                                    <div>
                                        <h4>Capacité</h4>
                                    </div>
                                    <button type="button" class="btn btn-sm btn-primary" onclick="analyser('CAPACITE');">
                                        Analyser
                                    </button>
                                </div>
                                <div class="row">
                                    <!-- Column starts -->
                                    <div class="col-xl-12">
                                        <div class="card dz-card" id="accordion-one">
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="Preview" role="tabpanel"
                                                    aria-labelledby="home-tab">
                                                    <div class="card-body pt-0">
                                                        <div class="table-responsive">
                                                            <table id="capacite_datatable" class="display table"
                                                                style="min-width: 80%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        @foreach ($capacite_header as $item)
                                                                            <th>{{ $item }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($capacite as $item)
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>{{ $item->bcfname }}</td>
                                                                            <td>{{ $item->btsname }}</td>
                                                                            <td>{{ $item->sitecode }}</td>
                                                                            <td>{{ $item->detected }}</td>
                                                                            <td>{{ $item->value1 }}</td>
                                                                            <td>{{ $item->value2 }}</td>
                                                                            <td>{{ $item->value3 }}</td>
                                                                            <td>{{ $item->value4 }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        @foreach ($capacite_header as $item)
                                                                            <th>{{ $item }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /Default accordion -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="qualite">
                            <div class="pt-4">
                                <div class="card-header flex-wrap d-flex justify-content-between">
                                    <div>
                                        <h4>Qualité</h4>
                                    </div>
                                    <button type="button" class="btn btn-sm btn-primary" onclick="analyser('QUALITE');">
                                        Analyser
                                    </button>
                                </div>
                                <div class="row">
                                    <!-- Column starts -->
                                    <div class="col-xl-12">
                                        <div class="card dz-card" id="accordion-one">
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="Preview" role="tabpanel"
                                                    aria-labelledby="home-tab">
                                                    <div class="card-body pt-0">
                                                        <div class="table-responsive">
                                                            <table id="qualite_datatable" class="display table"
                                                                style="max-width: 100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        @foreach ($qualite_header as $item)
                                                                            <th>{{ $item }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($qualite as $item)
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>{{ $item->bcfname }}</td>
                                                                            <td>{{ $item->btsname }}</td>
                                                                            <td>{{ $item->sitecode }}</td>
                                                                            <td>{{ $item->detected }}</td>
                                                                            <td>{{ $item->value1 }}</td>
                                                                            <td>{{ $item->value2 }}</td>
                                                                            <td>{{ $item->value3 }}</td>
                                                                            <td>{{ $item->value4 }}</td>
                                                                            <td>{{ $item->value5 }}</td>
                                                                            <td>{{ $item->value6 }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        @foreach ($qualite_header as $item)
                                                                            <th>{{ $item }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- /Default accordion -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="transport">
                            <div class="pt-4">
                                <h4>Transport</h4>
                                <div class="card">
                                    <div class="card-body pt-0">
                                        <div class="table-responsive">
                                            <table id="transport_datatable" class="display table" style="max-width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        @foreach ($transport_header as $item)
                                                            <th>{{ $item }}</th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($transport as $item)
                                                        <tr>
                                                            <td><i class="bi bi-eye" style="color:blue"
                                                                    onclick="transportElementAnalyse();"></i></td>
                                                            <td>{{ $item->bcfname }}</td>
                                                            <td>{{ $item->btsname }}</td>
                                                            <td>{{ $item->sitecode }}</td>
                                                            <td>{{ $item->detected }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>#</th>
                                                        @foreach ($transport_header as $item)
                                                            <th>{{ $item }}</th>
                                                        @endforeach
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ANALYSE INFO -->
    <button id="info_analyse_btn" type="button" class="" data-bs-toggle="modal" data-bs-target="#info_analyse"
        hidden></button>
    <div class="modal fade" id="info_analyse">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Details d'analyse</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal">
                    </button>
                </div>
                <div id="info_analyse_body" class="modal-body">

                </div>
                <div class="modal-footer">
                    <div class="accordion accordion-primary" id="accordion-one">
                        <div class="accordion-item">
                            <div class="accordion-header collapsed rounded-lg" id="accord-2Two" data-bs-toggle="collapse"
                                data-bs-target="#collapse2Two" aria-controls="collapse2Two" aria-expanded="true"
                                role="button">
                                <span class="accordion-header-text">Orientation</span>
                            </div>
                            <div id="collapse2Two" class="collapse accordion__body" aria-labelledby="accord-2Two"
                                data-bs-parent="#accordion-two">
                                <div id="orientation_body" class="accordion-body-text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('table.display').DataTable();
        });
    </script>
    <script src="{{ asset('assets/js/custom_analyser.js') }}"></script>
@endsection
