@extends('layouts.app')

@section('content')
    <div class="card dz-card" id="accordion-two">
        <div class="card-header flex-wrap d-flex justify-content-between">
            <div></div>
            <a href="{{ route('user.analyse.file', ['file' => $filename]) }}">
                <button type="button" class="btn btn-sm btn-primary">
                    Detection
                </button>
            </a>
        </div>

        <!-- tab-content -->
        <div class="tab-content" id="myTabContent-1">
            <div class="tab-pane fade show active" id="bordered" role="tabpanel" aria-labelledby="home-tab-1">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="display table" style="width:100%">
                            <thead>
                                <tr>
                                    @foreach ($header as $col)
                                        <th>{{ $col }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                    <tr>
                                        <td>{{ $row->date }}</td>
                                        <td>{{ $row->sitename }}</td>
                                        <td>{{ $row->sitecode }}</td>
                                        <td>{{ $row->longitude }}</td>
                                        <td>{{ $row->latitude }}</td>
                                        <td>{{ $row->param1 }}</td>
                                        <td>{{ $row->param2 }}</td>
                                        <td>{{ $row->param3 }}</td>
                                        <td>{{ $row->param4 }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    @foreach ($header as $col)
                                        <th>{{ $col }}</th>
                                    @endforeach
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
