<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="robots" content="">

    <!-- PAGE TITLE HERE -->
    <title>DAO App</title>
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/data/images/favicon.png') }}">

    <link href="{{ asset('assets/css/site/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/site/swiper-bundle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/css2e91f.css?family=Material+Icons') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/nouislider.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/site/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/site/jquery-jvectormap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/buttons.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/site/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <!-- tagify-css -->
    <link href="{{ asset('assets/css/site/tagify.css') }}" rel="stylesheet">

    <!-- Style css -->
    <link href="{{ asset('assets/css/site/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
</head>

<body data-typography="poppins" data-theme-version="light" data-layout="vertical" data-nav-headerbg="black"
    data-headerbg="color_1">

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header import
        ***********************************-->
        @include('layouts.header')

        <!--**********************************
            Sidebar import
        ***********************************-->
        @include('layouts.sidebar')

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <!--**********************************
                Page Title import
            ***********************************-->
            @include('layouts.page_title')
            <div class="container-fluid">
                <div class="row">
                    @if (Session::get('message'))
                        <div
                            class="alert alert-{{ Session::get('alert-class') ?? 'info' }} alert-dismissible fade show">
                            <strong>{{ Session::get('message') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                <span><i class="fa-solid fa-xmark"></i></span>
                            </button>
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer import
        ***********************************-->
        @include('layouts.footer')
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{ asset('assets/js/site/global.min.js') }}"></script>
    <script src="{{ asset('assets/js/site/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/site/bootstrap-select.min.js') }}"></script>

    <!-- tagify -->
    <script src="{{ asset('assets/js/site/tagify.js') }}"></script>

    <script src="{{ asset('assets/js/site/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/site/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/site/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/site/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/site/datatables.init.js') }}"></script>

    <!-- Apex Chart -->
    <script src="{{ asset('assets/js/site/moment.js') }}"></script>
    <script src="{{ asset('assets/js/site/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('assets/js/site/custom.js') }}"></script>
    <script src="{{ asset('assets/js/site/deznav-init.js') }}"></script>
    <script src="{{ asset('assets/js/site/demo.js') }}"></script>

    @yield('script')
</body>

</html>
