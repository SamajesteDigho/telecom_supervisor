@if (isset($breadCrumb))
    <div class="page-titles mb-2">
        <ol class="breadcrumb">
            <li>
                <h5 class="bc-title">{{ $breadCrumb->name ?? '' }}</h5>
            </li>
            @foreach ($breadCrumb->levels ?? [] as $level)
                <span> / </span>
                <li class="breadcrumb-ite">
                    <a href="javascript:void(0)">
                        {{ $level ?? '' }}
                    </a>
                </li>
            @endforeach
        </ol>
    </div>
@endif
