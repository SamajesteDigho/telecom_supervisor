@extends('layouts.app')

@section('content')
    <script>
        var DATA_SET = @json($data);
        var INTERVAL = @json($intervals);
        var CATEGORIZED_DATA = {};
        var FILE_NAME = @json($file_path);
        var USER_ID = @json(Auth::user()->id);
        var GENERATION = @json($generation);
    </script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    Total N° of entries : <strong>{{ count($data) }}</strong>
                </div>
                <div class="nav flex-column nav-pills mb-3">
                    @foreach (array_keys($intervals) as $item)
                        <a href="#{{ $intervals[$item]->ID }}" data-bs-toggle="pill"
                            class="nav-link {{ $intervals[$item]->ID == 'pb1' ? 'active' : '' }}">
                            {{ $intervals[$item]->NAME }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-lg-8 col-xl-8">
            <div class="tab-content">
                @foreach (array_keys($intervals) as $item)
                    @include('analyse.problem_tmp', ['PARAM' => $intervals[$item]])
                @endforeach
            </div>
        </div>
    </div>

    <!-- Statistics and Map -->
    <div class="row">
        <div class="col-xl-4 col-lg-4">
            <div class="card">
                <div class="card-body">
                    Statistiques d'Analyse
                    <div class="table-responsive">
                        <table class="table header-border">
                            <thead>
                                <tr>
                                    <th>Label</th>
                                    <th>Number</th>
                                    <th>Show</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stat_entities as $entity)
                                    <tr class="{{ 'table-' . $entity['class'] }}">
                                        <td><span class="{{ $entity['color'] . '-dot' }}"></span> {{ $entity['label'] }}
                                        </td>
                                        <td>
                                            <span id="{{ $entity['short'] }}_value"></span>
                                            <span id="{{ $entity['short'] }}_percent"></span>
                                            <div class="progress" style="background: white">
                                                <div id="{{ $entity['short'] }}_prop" class="progress-bar bg-primary"
                                                    role="progressbar">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary shadow btn-xs sharp me-1 mb-2"
                                                data-bs-toggle="modal"
                                                data-bs-target="{{ '#modal_' . $entity['param'] . '_Data' }}"
                                                onclick="displayCategorizedData('{{ $entity['param'] }}');">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                            <div class="modal fade" id="{{ 'modal_' . $entity['param'] . '_Data' }}">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Data Display</h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal">
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table id="{{ 'example-modal_data_' . $entity['param'] }}"
                                                                class="display table" style="min-width: 80%">
                                                                <thead>
                                                                    <tr class="table-info">
                                                                        <td>#</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary btn-sm"
                                                                onclick="downloadDataToExcel('{{ $entity['param'] }}');">Download</button>

                                                            <button type="button" class="btn btn-success btn-sm"
                                                                onclick="saveDataToExcelInServer('{{ $entity['param'] }}');">Sauvegarder</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row my-2">
                        <button type="button" class="btn btn-sm btn-success shadow sharp mb-2" data-bs-toggle="modal"
                            data-bs-target="#generalStatisticsModal" onclick="showGlobalStatistics();">
                            Statistics Global
                        </button>
                        <div class="modal fade" id="generalStatisticsModal">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Statistics Globales</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal">
                                        </button>
                                    </div>
                                    <div class="modal-body" id="generalstats-modal-data">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-8">
            <div class="card overflow-hidden">
                <div class="card-header border-0">
                    <h4 class="heading mb-0">Carte des Sites</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-9">
                            <div id="mappy" style="height:400px;">
                            </div>
                            <script type="text/javascript" async defer
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQmys_DL3Mw8VFWOwCVG28uDZQCJjKVUU&callback=initMap"></script>
                        </div>
                        <!--<div class="col-xl-3 active-country dz-scroll">
                                                                <div class="country-list">
                                                                    <div class="progress-box mt-0">
                                                                        <div class="d-flex justify-content-between">
                                                                            <p class="mb-0 c-name">
                                                                                <span class="green-dot"></span>
                                                                                Très Bonne Qualité
                                                                            </p>
                                                                            <p class="mb-0" id="map_vgq_value"></p>
                                                                        </div>
                                                                        <div class="progress">
                                                                            <div id="map_vgq_percent" class="progress-bar bg-primary"
                                                                                style="height:5px; border-radius:4px;" role="progressbar">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="country-list">
                                                                    <div class="progress-box mt-0">
                                                                        <div class="d-flex justify-content-between">
                                                                            <p class="mb-0 c-name">
                                                                                <span class="blue-dot"></span>
                                                                                Bonne Qualité
                                                                            </p>
                                                                            <p class="mb-0" id="map_gq_value"></p>
                                                                        </div>
                                                                        <div class="progress">
                                                                            <div id="map_gq_percent" class="progress-bar bg-blue"
                                                                                style="height:5px; border-radius:4px;" role="progressbar">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="country-list">
                                                                    <div class="progress-box mt-0">
                                                                        <div class="d-flex justify-content-between">
                                                                            <p class="mb-0 c-name">
                                                                                <span class="orange-dot"></span>
                                                                                Assez Bonne Qualité
                                                                            </p>
                                                                            <p class="mb-0" id="map_avg_value"></p>
                                                                        </div>
                                                                        <div class="progress">
                                                                            <div id="map_avg_percent" class="progress-bar bg-warning"
                                                                                style="height:5px; border-radius:4px;" role="progressbar">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="country-list">
                                                                    <div class="progress-box mt-0">
                                                                        <div class="d-flex justify-content-between">
                                                                            <p class="mb-0 c-name">
                                                                                <span class="red-dot"></span>
                                                                                Mauvaise Qualité
                                                                            </p>
                                                                            <p class="mb-0" id="map_bad_value"></p>
                                                                        </div>
                                                                        <div class="progress">
                                                                            <div id="map_bad_percent" class="progress-bar bg-danger"
                                                                                style="height:5px; border-radius:4px;" role="progressbar">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/js/xlsx.min.js') }}"></script>
@endsection
