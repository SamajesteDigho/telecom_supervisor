@extends('layouts.app')

@section('content')
    <div class="col-xl-12 bst-seller">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive active-projects style-1 ItemsCheckboxSec shorting ">
                    <div class="tbl-caption">
                        <h4 class="heading mb-0">Employees</h4>
                    </div>
                    <table id="empoloyees-tbl" class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>File Name</th>
                                <th>Imported By</th>
                                <th>Type</th>
                                <th>Generation</th>
                                <th>Created at</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($files as $file)
                                <tr>
                                    <td>
                                        <span>{{ $file->id }}</span>
                                    </td>
                                    <td>
                                        <span>
                                            <a
                                                href="{{ route('user.analyse.file', ['file' => explode('.', $file->filename)[0]]) }}">
                                                <li class="li-item"> {{ explode('.', $file->filename)[0] }} </li>
                                            </a>
                                        </span>
                                    </td>
                                    <td><span>{{ $file->user_id }}</span></td>
                                    <td><span class="badge badge-success light border-0">{{ $file->type }}</span></td>
                                    <td><span>{{ $file->generation }}</span></td>
                                    <td><span>{{ $file->created_at }}</span></td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="{{ route('user.import.display', ['filename' => explode('.', $file->filename)[0]]) }}"
                                                class="btn btn-primary shadow btn-xs sharp me-1"><i
                                                    class="fa fa-eye"></i></a>
                                            <a href="#" class="btn btn-danger shadow btn-xs sharp"><i
                                                    class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
