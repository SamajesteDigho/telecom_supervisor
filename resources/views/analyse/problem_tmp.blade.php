<div id="{{ $PARAM->ID }}" class="tab-pane fade {{ $PARAM->ID == 'pb1' ? 'show active' : '' }}">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ $PARAM->LABEL }} {{ $PARAM->MESURE == '' ? '' : '(' . $PARAM->MESURE . ')' }}
                </h4>
                <button type="button" class="btn btn-info btn-sm" onclick="displayStatistics('{{ $PARAM->ID }}');">
                    <strong>Detection {{ $generation }}</strong>
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <table class="table">
                            <tbody>
                                <col />
                                <col width="100px" />
                                <col width="100px" />
                                <col />
                                <col width="100px" />
                                <col width="100px" />
                                <tr>
                                    <td>
                                        <span class="green-dot"></span>
                                        <strong>Très bonne</strong>
                                    </td>
                                    <td>
                                        <input type="number" id="vgq_val_max_{{ $PARAM->ID }}"
                                            class="form-control col-sm-1 number-to-text number-at-center"
                                            value="{{ $PARAM->VERYGOOD_MAX }}">
                                    </td>
                                    <td>
                                        <input type="number" id="vgq_val_min_{{ $PARAM->ID }}"
                                            class="form-control col-sm-1 number-to-text number-at-center"
                                            value="{{ $PARAM->VERYGOOD_MIN }}">
                                    </td>
                                    <td>
                                        <span class="orange-dot"></span>
                                        <strong>Assez Bonne</strong>
                                    </td>
                                    <td>
                                        <input type="number" id="avg_val_max_{{ $PARAM->ID }}"
                                            class="form-control number-to-text number-at-center"
                                            value="{{ $PARAM->AVERAGE_MAX }}">
                                    </td>
                                    <td>
                                        <input type="number" id="avg_val_min_{{ $PARAM->ID }}"
                                            class="form-control number-to-text number-at-center"
                                            value="{{ $PARAM->AVERAGE_MIN }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="blue-dot"></span>
                                        <strong>Bonne</strong>
                                    </td>
                                    <td>
                                        <input type="number" id="gq_val_max_{{ $PARAM->ID }}"
                                            class="form-control number-to-text number-at-center"
                                            value="{{ $PARAM->GOOD_MAX }}">
                                    </td>
                                    <td>
                                        <input type="number" id="gq_val_min_{{ $PARAM->ID }}"
                                            class="form-control number-to-text number-at-center"
                                            value="{{ $PARAM->GOOD_MIN }}">
                                    </td>
                                    <td>
                                        <span class="red-dot"></span>
                                        <strong>Mauvaise</strong>
                                    </td>
                                    <td>
                                        <input type="number" id="bad_val_max_{{ $PARAM->ID }}"
                                            class="form-control number-to-text number-at-center"
                                            value="{{ $PARAM->BAD_MAX }}">
                                    </td>
                                    <td>
                                        <input type="number" id="bad_val_min_{{ $PARAM->ID }}"
                                            class="form-control number-to-text number-at-center"
                                            value="{{ $PARAM->BAD_MIN }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
