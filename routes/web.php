<?php

use App\Http\Controllers\AnalyseController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('auth/login');
});

Route::group(['prefix' => 'auth'], function () {
    Route::get('login', [AuthController::class, 'login'])->name('login');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('reset-password', [AuthController::class, 'reset_password'])->name('reset-password');
});

Route::group(['prefix' => 'users', 'middleware' => 'auth:sanctum'], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('user.dashboard');

    Route::group(['prefix' => 'account'], function () {
        Route::get('', [UserController::class, 'index'])->name('user.account.list');
        Route::get('create', [UserController::class, 'create'])->name('user.account.create');
        Route::post('store', [UserController::class, 'store'])->name('user.account.store');
    });

    Route::group(['prefix' => 'import'], function () {
        Route::get('', [ImportController::class, 'index'])->name('user.import');
        Route::post('upload', [ImportController::class, 'upload'])->name('user.import.upload');
        Route::get('ftp_upload', [ImportController::class, 'ftp_upload'])->name('user.import.ftp_upload');
        Route::post('ftp_upload', [ImportController::class, 'ftp_upload'])->name('user.import.ftp_upload');
        Route::get('display/{filename}', [ImportController::class, 'display'])->name('user.import.display');
    });

    Route::group(['prefix' => 'analyse'], function () {
        Route::get('', [AnalyseController::class, 'index'])->name('user.analyse');
        Route::get('/file/{file}', [AnalyseController::class, 'analyse_file'])->name('user.analyse.file');
    });

    Route::group(['prefix' => 'advanced_analyse'], function () {
        Route::get('', [AnalyseController::class, 'advanced_index'])->name('user.advanced_analysis');
        Route::post('upload', [AnalyseController::class, 'advanced_import'])->name('user.advanced_analysis.upload');
        Route::get('treatement', [AnalyseController::class, 'advanced_treatement'])->name('user.advanced_analysis.treatement');
    });
});