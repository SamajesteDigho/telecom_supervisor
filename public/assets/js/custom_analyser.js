COUVERTURE_SIMPLIFIED_DATA = [];
CAPACITE_SIMPLIFIED_DATA = [];
QUALITE_SIMPLIFIED_DATA = [];

function analyser(type) {
    if (type == 'COUVERTURE') {
        data = COUVERTURE;
        TRANSITION = {};
        COUVERTURE_SIMPLIFIED_DATA = [];
        data.forEach(element => {
            if (TRANSITION[element.sitecode] === undefined) {
                TRANSITION[element.sitecode] = [];
            }
            TRANSITION[element.sitecode].push(element);
        });
        for (const [key, value] of Object.entries(TRANSITION)) {
            COUVERTURE_SIMPLIFIED_DATA.push(value.reduce((prev, curr) => Number(prev.value7) < Number(curr.value7) ? prev : curr));
        }
        createDataTableAndAppend(type);
    }

    else if (type == 'CAPACITE') {
        data = CAPACITE;
        TRANSITION = {};
        CAPACITE_SIMPLIFIED_DATA = [];
        data.forEach(element => {
            if (TRANSITION[element.sitecode] === undefined) {
                TRANSITION[element.sitecode] = [];
            }
            TRANSITION[element.sitecode].push(element);
        });
        for (const [key, value] of Object.entries(TRANSITION)) {
            CAPACITE_SIMPLIFIED_DATA.push(value.reduce((prev, curr) => Number(prev.value1) > Number(curr.value1) ? prev : curr));
        }
        createDataTableAndAppend(type);
    }

    else if (type == 'QUALITE') {
        data = QUALITE;
        TRANSITION = {};
        QUALITE_SIMPLIFIED_DATA = [];
        data.forEach(element => {
            if (TRANSITION[element.sitecode] === undefined) {
                TRANSITION[element.sitecode] = [];
            }
            TRANSITION[element.sitecode].push(element);
        });
        for (const [key, value] of Object.entries(TRANSITION)) {
            QUALITE_SIMPLIFIED_DATA.push(value.reduce((prev, curr) => Number(prev.value1) < Number(curr.value1) ? prev : curr));
        }
        createDataTableAndAppend(type);
    }
}

function createDataTableAndAppend(probleme) {
    if (probleme == 'COUVERTURE') {
        var dt = $('#couverture_datatable').DataTable();
        dt.clear();
        array = COUVERTURE_SIMPLIFIED_DATA;
        array.forEach((element, index) => {
            eye = "<i class='bi bi-eye' style='color:blue;' onclick='couvertureElementAnalyse(" + index + ");'></i>";
            dt.row.add([eye, element.bcfname, element.btsname, element.sitecode, element.detected, element.value7, element.value1, element.value2, element.value3, element.value4, element.value5, element.value6]).draw(true);
        });
    }
    else if (probleme == 'CAPACITE') {
        var dt = $('#capacite_datatable').DataTable();
        dt.clear();
        array = CAPACITE_SIMPLIFIED_DATA;
        array.forEach((element, index) => {
            eye = "<i class='bi bi-eye' style='color:blue;' onclick='capaciteElementAnalyse(" + index + ");'></i>";
            dt.row.add([eye, element.bcfname, element.btsname, element.sitecode, element.detected, element.value1, element.value2, element.value3, element.value4]).draw(true);
        });
    }
    else if (probleme == 'QUALITE') {
        var dt = $('#qualite_datatable').DataTable();
        dt.clear();
        array = QUALITE_SIMPLIFIED_DATA;
        array.forEach((element, index) => {
            eye = "<i class='bi bi-eye' style='color:blue;' onclick='qualiteElementAnalyse(" + index + ");'></i>";
            dt.row.add([eye, element.bcfname, element.btsname, element.sitecode, element.detected, element.value1, element.value2, element.value3, element.value4, element.value5, element.value6]).draw(true);
        });
    }
}

function couvertureElementAnalyse(id) {
    btsname = COUVERTURE_SIMPLIFIED_DATA[id].btsname;
    sitecode = COUVERTURE_SIMPLIFIED_DATA[id].sitecode;
    value = COUVERTURE_SIMPLIFIED_DATA[id].value7;
    if (Number(value) <= -95) {
        document.getElementById('info_analyse_body').innerHTML = "Site potentiellement isolé ayant un grand rayon de couverture."
        document.getElementById('orientation_body').innerHTML =
            "- Vérifier la configuration des arriérés (tilts éléctriques, hauteur de l'entenne).<br><br>"
            + "- Planifier un nouveau site en cas de configuration optimale des arriérés.";
    }
    else if (Number(value) > -95) {
        document.getElementById('info_analyse_body').innerHTML = "Faible proportion d'utilisateurs mal couverts.";
        document.getElementById('orientation_body').innerHTML =
            "- Vérifier les distances de couveture de la cellule pour vérifier qu'elle couvre bien dans sa zone.<br><br>"
            + "- Vérifier la distance d'intensité.<br><br>"
            + "- Proposer un downtilt au besoin.";
    }
    else {
        document.getElementById('info_analyse_body').innerHTML = 'Aucune Analyse';
        document.getElementById('orientation_body').innerHTML = "Pas d'orientation.";
    }
    $('#info_analyse_btn').click();
}

function capaciteElementAnalyse(id) {
    btsname = CAPACITE_SIMPLIFIED_DATA[id].btsname;
    bcfname = CAPACITE_SIMPLIFIED_DATA[id].bcfname;
    value3 = CAPACITE_SIMPLIFIED_DATA[id].value3;
    value4 = CAPACITE_SIMPLIFIED_DATA[id].value4;
    if (Number(value3) > 0 || Number(value4) > 0) {
        document.getElementById('info_analyse_body').innerHTML = "Pas de congestion TCH et SDCCH";
        document.getElementById('orientation_body').innerHTML =
            "- Ajout TRX<br><br>"
            + "- Traffic steering vers la 3G.";
    }
    else {
        document.getElementById('info_analyse_body').innerHTML = "Congestion";
        document.getElementById('orientation_body').innerHTML =
            "- Ajout TRX.<br><br>"
            + "- Traffic steering vers la 3G.<br><br>"
            + "- Ajout de Site.";
    }
    $('#info_analyse_btn').click();
}

function qualiteElementAnalyse(id) {
    btsname = QUALITE_SIMPLIFIED_DATA[id].btsname;
    bcfname = QUALITE_SIMPLIFIED_DATA[id].bcfname;
    band1 = Number(QUALITE_SIMPLIFIED_DATA[id].value1);
    band2 = Number(QUALITE_SIMPLIFIED_DATA[id].value2);
    band3 = Number(QUALITE_SIMPLIFIED_DATA[id].value3);
    band4 = Number(QUALITE_SIMPLIFIED_DATA[id].value4);
    band5 = Number(QUALITE_SIMPLIFIED_DATA[id].value5);
    if ([band1, band2].includes(Math.max(band1, band2, band3, band4, band5))) {
        document.getElementById('info_analyse_body').innerHTML = "Pas d'interference externe.";
        document.getElementById('orientation_body').innerHTML =
            "- Vérifier la planification fréquentielle (Vérifier le cas d'interférence coc-canal).<br><br>"
            + "- Vérifier les interference cannaux adjacents.";
    }
    else if ([band3, band4, band5].includes(Math.max(band1, band2, band3, band4, band5))) {
        document.getElementById('info_analyse_body').innerHTML = 'Interference externe.';
        document.getElementById('orientation_body').innerHTML =
            "- Faire un scan de la zone afin de detecter la source.<br><br>"
            + "- Vérifier le fonctionnement physique des équipements.";
    }
    $('#info_analyse_btn').click();
}

function transportElementAnalyse() {
    document.getElementById('info_analyse_body').innerHTML = "Problème de transport causé par la surcharge.";
    document.getElementById('orientation_body').innerHTML = "Augmenter la capacité des lignes transport.";
    $('#info_analyse_btn').click();
}