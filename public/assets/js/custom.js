function initMap(pbID='') {
    try {
        var infoWindow = new google.maps.InfoWindow();
        // Map options
        var options = {
            zoom: 8,
            scrollwheel: true,
            center: {
                lat: 6.189733436912043,
                lng: 12.516494357749997
            },

        };

        // New map
        var map = new google.maps.Map(document.getElementById('mappy'), options);

        if (Object.keys(CATEGORIZED_DATA).length == 0) {
            console.log('No data to plot');
        }
        else {
            CATEGORIZED_DATA[pbID].very_good.forEach((site) => {
                addMarker(site, pbID, 'VERY GOOD', 'http://maps.google.com/mapfiles/ms/icons/green-dot.png');
            });
            CATEGORIZED_DATA[pbID].good.forEach((site) => {
                addMarker(site, pbID, 'GOOD', 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            });
            CATEGORIZED_DATA[pbID].average.forEach((site) => {
                addMarker(site, pbID, 'AVERAGE', 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
            });
            CATEGORIZED_DATA[pbID].bad.forEach((site) => {
                addMarker(site, pbID, 'VERY BAD', 'http://maps.google.com/mapfiles/ms/icons/red-dot.png');
            });

        }

        // Add Marker function
        function addMarker(site, param, state, icon) {
            var marker = new google.maps.Marker({
                position: {
                    lat: Number(site.longitude.replace(',', '.')),
                    lng: Number(site.latitude.replace(',', '.'))
                },
                map: map,
                icon: {url: icon}
            });

            let value;
            let pb;
            if (param == 'pb1') {
                value = site.param1;
                pb = INTERVAL.PROBLEM1;
            }
            else if (param == 'pb2') {
                value = site.param2;
                pb = INTERVAL.PROBLEM2;
            }
            else if (param == 'pb3') {
                value = site.param3;
                pb = INTERVAL.PROBLEM3;
            }
            else if (param == 'pb4') {
                value = site.param4;
                pb = INTERVAL.PROBLEM4;
            }
            marker.addListener('click', function() {
                var content = '<h3>' + site.sitename + '(' + site.sitecode + ')</h3>' +
                    '<ul><li>Value Analysed: ' + value + '</li>'+
                    '<ul><li>Parameter : ' + pb.NAME + '</li>'+
                    '<li>State : ' + state + '</li></ul>';
                infoWindow.setContent(content);
                infoWindow.open(map, marker);
            });
        }
    }
    catch(e) {
        console.log(e);
    }
}

function displayStatistics(pbID) {
    CATEGORIZED_DATA[pbID] = divideIntoQualityType(DATA_SET, pbID);
    CATEGORIZED_DATA['parameter'] = pbID;
    // display the statistics
    setDataDisplay(CATEGORIZED_DATA[pbID]);
    initMap(pbID);
    alert('Les statistique ont été calculées.');
    postAnalysis(pbID);
}

function divideIntoQualityType(data, pbID) {
    interval = {
        'VERY_GOOD_MIN': Number(document.getElementById('vgq_val_min_' + pbID).value || '0'),
        'VERY_GOOD_MAX': Number(document.getElementById('vgq_val_max_' + pbID).value || '0'),
        'GOOD_MIN': Number(document.getElementById('gq_val_min_' + pbID).value || '0'),
        'GOOD_MAX': Number(document.getElementById('gq_val_max_' + pbID).value || '0'),
        'AVERAGE_MIN': Number(document.getElementById('avg_val_min_' + pbID).value || '0'),
        'AVERAGE_MAX': Number(document.getElementById('avg_val_max_' + pbID).value || '0'),
        'BAD_MIN': Number(document.getElementById('bad_val_min_' + pbID).value || '0'),
        'BAD_MAX': Number(document.getElementById('bad_val_max_' + pbID).value || '0'),
    };

    very_good = [];
    good = [];
    average = [];
    bad = [];
    nan = [];

    data.forEach(element => {
        value = 0;
        // Selecting the parrameter
        if(pbID == 'pb1') {
            value = Number((element.param1 || '0').replace(',', '.'));
        }
        else if(pbID == 'pb2') {
            value = Number((element.param2 || '0').replace(',', '.'));
        }
        else if(pbID == 'pb3') {
            value = Number((element.param3 || '0').replace(',', '.'));
        }
        else if(pbID == 'pb4') {
            value = Number((element.param4 || '0').replace(',', '.'));
        }
        else if(pbID == 'pb5') {
            value = Number((element.param5 || '0').replace(',', '.'));
        }
        else if(pbID == 'pb6') {
            value = Number((element.param6 || '0').replace(',', '.'));
        }

        // Distributing the data
        if (value >= interval.VERY_GOOD_MIN && value <= interval.VERY_GOOD_MAX) {
            very_good.push(element);
        } else if (value >= interval.GOOD_MIN && value <= interval.GOOD_MAX) {
            good.push(element);
        } else if (value >= interval.AVERAGE_MIN && value <= interval.AVERAGE_MAX) {
            average.push(element);
        } else if (value >= interval.BAD_MIN && value <= interval.BAD_MAX) {
            bad.push(element);
        } else {
            nan.push(element);
        }
    });
    return {
        'very_good': very_good,
        'good': good,
        'average': average,
        'bad': bad,
        'nan': nan,
        'total_shared_length': very_good.length + good.length + average.length + bad.length,
        'total_data_length': data.length,
    };
}

function setDataDisplay(data_segment) {
    // display properties for very good
    size = data_segment.very_good.length ?? 0;
    data_segment.total_shared_length == 0 ? percent = 0 : percent = (size / data_segment.total_shared_length * 100).toFixed(2);
    document.getElementById('vgq_value').innerHTML = size;
    document.getElementById('vgq_percent').innerHTML = '[' + percent + '%]';
    document.getElementById('vgq_prop').style.cssText = 'width:' + percent + '%;';
   
    // display properties for good
    size = data_segment.good.length ?? 0;
    data_segment.total_shared_length == 0 ? percent = 0 : percent = (size / data_segment.total_shared_length * 100).toFixed(2);
    document.getElementById('gq_value').innerHTML = size;
    document.getElementById('gq_percent').innerHTML = '[' + percent + '%]';
    document.getElementById('gq_prop').style.cssText = 'width:' + percent + '%;';
   
    // display properties for Average
    size = data_segment.average.length ?? 0;
    data_segment.total_shared_length == 0 ? percent = 0 : percent = (size / data_segment.total_shared_length * 100).toFixed(2);
    document.getElementById('avg_value').innerHTML = size;
    document.getElementById('avg_percent').innerHTML = '[' + percent + '%]';
    document.getElementById('avg_prop').style.cssText = 'width:' + percent + '%;';
 
    // display properties for Bad
    size = data_segment.bad.length ?? 0;
    data_segment.total_shared_length == 0 ? percent = 0 : percent = (size / data_segment.total_shared_length * 100).toFixed(2);
    document.getElementById('bad_value').innerHTML = size;
    document.getElementById('bad_percent').innerHTML = '[' + percent + '%]';
    document.getElementById('bad_prop').style.cssText = 'width:' + percent + '%;';
 }

function displayCategorizedData(state) {
    var dt = $('#example-modal_data_' + state).DataTable();
    dt.clear();
    if (!CATEGORIZED_DATA.hasOwnProperty(CATEGORIZED_DATA.parameter)) {
        // document.getElementById('modal-'+ state +'-data').innerHTML = "L'analyse n'a pas encore été effectué. Veillez faire l'analyse avant d'afficher les categories.";
        return ;
    }
    parameter = CATEGORIZED_DATA.parameter;
    $(dt.columns([1]).header()).html('Site Name');
    $(dt.columns([2]).header()).html('Site Code');
    $(dt.columns([3]).header()).html(getParameterAttribut(parameter).NAME || 'Parameter');
    // $(dt.columns([4]).header()).html('Comming');
    
    var dataset = [];
    if (state == 'very_good') {
        dataset = CATEGORIZED_DATA[CATEGORIZED_DATA['parameter']].very_good;
    }
    else if(state == 'good') {
        dataset = CATEGORIZED_DATA[CATEGORIZED_DATA['parameter']].good;
    }
    else if (state == 'average') {
        dataset = CATEGORIZED_DATA[CATEGORIZED_DATA['parameter']].average;
    }
    else {
        dataset = CATEGORIZED_DATA[CATEGORIZED_DATA['parameter']].bad;
    }

    // Apply rows
    var count = 1;
    dataset.forEach(element => {
        let value;
        if (parameter == 'pb1') {
            value = element.param1;
        }
        else if (parameter == 'pb2') {
            value = element.param2;
        }
        else if (parameter == 'pb3') {
            value = element.param3;
        }
        else if (parameter == 'pb4') {
            value = element.param4;
        }
        dt.row.add([count, element.sitename, element.sitecode, Number(value).toFixed(2), '...']).draw(false);
        count++;
    });
}

function showGlobalStatistics() {
    intern = document.querySelector("#generalstats-modal-data");
    if (Object.keys(CATEGORIZED_DATA).length == 0) {
        intern.innerHTML = "<p> No Statistical data present. </p>";
        return ;
    }

    problems = [
        {'id': 'pb1', 'name': 'Couverture'},
        {'id': 'pb2', 'name': 'Qualité Signal'},
        {'id': 'pb3', 'name': 'Capacité Réseau'},
        {'id': 'pb4', 'name': 'Transport'}
    ];
    states = [
        {'id': 'very_good', 'name': 'TRES BONNE'},
        {'id': 'good', 'name': 'BONNE'},
        {'id': 'average', 'name': 'MOYENNE'},
        {'id': 'bad', 'name': 'MAUVAISE'},
        {'id': 'nan', 'name': 'OUTBOUNDS'},
        {'id': 'total_data_length', 'name': 'TOTAL'}
    ];

    table = document.createElement('table');
    table.setAttribute('class', 'table ItemsCheckboxSec primary-table-bordered');
    table.setAttribute('id', 'modal-data-table');

    problems.forEach((item) => {
        tr = document.createElement('tr');
        th1 = document.createElement('th');
        th1.setAttribute('class', 'table-info');
        th1.innerHTML = item.name;
        tr.appendChild(th1);
        states.forEach((col) => {
            cell = document.createElement('td');
            cell.setAttribute('class', 'text-center text-bold');
            try {
                if(col.id == 'total_data_length') {
                    cell.innerHTML = CATEGORIZED_DATA[item.id]?.[col.id];
                }
                else {
                    cell.innerHTML = CATEGORIZED_DATA[item.id]?.[col.id].length;
                }
            }
            catch (e) {
                cell.innerHTML = 'Non défini';
            }
            tr.appendChild(cell);
        });
        table.appendChild(tr);
    });

    thead = document.createElement('thead');
    tr = document.createElement('tr');
    tr.setAttribute('class', 'thead-primary');

    th = document.createElement('th');
    th.innerHTML = 'Problèmes';
    tr.appendChild(th);

    states.forEach((item) => {
        cell = document.createElement('th');
        cell.innerHTML = item.name;
        tr.appendChild(cell);
    });
    thead.appendChild(tr);
    table.appendChild(thead);

    intern.innerHTML = '';
    intern.appendChild(table);
}

function postAnalysis(param) {
    body = {
        'filename': FILE_NAME,
        'user_id': USER_ID,
        'param_name' : param,
        'generation': GENERATION,
        'duration': 0,
    };
    fetch('/api/store_analysis', {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
        "Content-Type": "application/json",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(body),
    }).then((response) => {
        return response.json();
    }).then((data) => {
        console.log(data);
    });
}

function getParameterAttribut(pbID) {
    if (pbID == 'pb1') {
        return INTERVAL.PROBLEM1;
    }
    else if (pbID == 'pb2') {
        return INTERVAL.PROBLEM2;
    }
    else if (pbID == 'pb3') {
        return INTERVAL.PROBLEM3;
    }
    else if (pbID == 'pb4') {
        return INTERVAL.PROBLEM4;
    }
}

function downloadDataToExcel(state) {
    jsonData = [];
    let param_name = getParameterAttribut(CATEGORIZED_DATA.parameter).LABEL;
    CATEGORIZED_DATA[CATEGORIZED_DATA.parameter][state].forEach((row) => {
        data = {
            'sitename': row.sitename,
            'sitecode': row.sitecode,
            'latitude': row.latitude,
            'longitude': row.longitude,
            param_name: row.param1
        }
        jsonData.push(data);
    });
    var jsonDataObject = eval(jsonData);
    exportWorksheet(jsonDataObject, param_name);
}

function exportWorksheet(jsonObject, name) {
    var myFile = "degrade_" + name + ".xlsx";
    var myWorkSheet = XLSX.utils.json_to_sheet(jsonObject);
    var myWorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(myWorkBook, myWorkSheet, "myWorkSheet");
    XLSX.writeFile(myWorkBook, myFile);
}

function saveDataToExcelInServer(state) {
    jsonData = [];
    alert(state);
    try{
        var data = CATEGORIZED_DATA[CATEGORIZED_DATA.parameter][state];
        let param_name = getParameterAttribut(CATEGORIZED_DATA.parameter).LABEL;
        CATEGORIZED_DATA[CATEGORIZED_DATA.parameter][state].forEach((row) => {
            if(CATEGORIZED_DATA.parameter == "pb1"){
                value = row.param1;
            }
            else if(CATEGORIZED_DATA.parameter == "pb2"){
                value = row.param2;
            }
            else if(CATEGORIZED_DATA.parameter == "pb3"){
                value = row.param3;
            }
            else if(CATEGORIZED_DATA.parameter == "pb4"){
                value = row.param4;
            }
            else {
                value = 0;
            }
            data = {
                'sitename': row.sitename,
                'sitecode': row.sitecode,
                'latitude': row.latitude,
                'longitude': row.longitude,
                param_name: value
            }
            jsonData.push(data);
        });
        var myFile = '';
        if(CATEGORIZED_DATA.parameter == 'pb1') {
            myFile = GENERATION + '_Couverture' + '.xlsx';
        }
        else if(CATEGORIZED_DATA.parameter == 'pb2') {
            myFile = GENERATION + '_Qualite' + '.xlsx';
        }
        else if(CATEGORIZED_DATA.parameter == 'pb3') {
            myFile = GENERATION + '_Capacite' + '.xlsx';
        }
        else if(CATEGORIZED_DATA.parameter == 'pb4') {
            myFile = GENERATION + '_Transport' + '.xlsx';
        }

        var myWorkSheet = XLSX.utils.json_to_sheet(eval(jsonData));
        var myWorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(myWorkBook, myWorkSheet, "myWorkSheet");

        var xlsblob = new Blob(
            [new Uint8Array(XLSX.write(myWorkBook, { bookType: "xlsx", type: "array" }))],
            {type:"application/octet-stream"}
        );
        
        var fd = new FormData();
        fd.append('file', xlsblob);
        fd.append('filename', myFile);

        fetch('/api/store_detection_file', {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            redirect: "follow",
            referrerPolicy: "no-referrer",
            body: fd,
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.error(error);
        });
    }
    catch(error){
        alert(error);
        return;
    }
}