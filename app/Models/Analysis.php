<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Analysis extends Model
{
    use HasFactory;

    protected $table = 'analysis';

    protected $fillable =  [
        'filename',
        'user_id',
        'param_name',
        'generation',
        'duration'
    ];
}
