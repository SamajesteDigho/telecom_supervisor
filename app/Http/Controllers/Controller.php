<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public const KPIS_FILE_DIRECTORY = 'assets/data/files/kpis/';
    public const TREATEMENT_FILE_DIRECTORY = 'assets/data/files/final/';
    public const PROCESSED_FILE_DIRECTORY = 'assets/data/files/processed/';
    public const SITE_TEMPLATE_FILE = 'assets/data/files/site_template.csv';
    public const SITE_2G_FILE = 'assets/data/files/sites/site_2G.xlsx';
    public const SITE_3G_FILE = 'assets/data/files/sites/site_3G.xlsx';

    public static function randString($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    static public function storeImage($image, string $objRef)
    {
        if (isset($image)) {
            $filename = $objRef . '.' . $image->extension();
            $path = 'assets/data/images/profile/';
            $image->move(public_path($path), $filename);
            return $path . '/' . $filename;
        }
        return null;
    }

    public static function saveFile($file, string $userRef, string $gen, string $state)
    {
        if (isset($file)) {
            $time = Carbon::now()->timestamp;
            $filename = "{$gen}_{$userRef}_{$time}.{$file->extension()}";
            $file->move(public_path('assets/data/files/' . $state), $filename);
            return $filename;
        }
        return null;
    }

    public static function saveFileFromName($file, $file_name, string $state)
    {
        if (isset($file)) {
            $filename = $file_name . $file->extension();
            $file->move(public_path('assets/data/files/' . $state), $filename);
            return $filename;
        }
        return null;
    }

    public static function saveDegradedFile($file, $file_name, string $state)
    {
        if (isset($file)) {
            $filename = $file_name;
            $file->move(public_path('assets/data/files/' . $state), $filename);
            return $filename;
        }
        return null;
    }


    public static function saveSiteTemplate($file, $generation)
    {
        if (isset($file)) {
            if ($generation == 2) {
                $file->move(public_path('assets/data/files/sites/'), 'site_2G.xlsx');
                return;
            }
            if ($generation == 3) {
                $file->move(public_path('assets/data/files/sites/'), 'site_3G.xlsx');
                return;
            }
        }
    }

    public static function openDataFile(string $file_path)
    {
        $data = Excel::toArray(new DataProperty, public_path($file_path))[0];
        $header = $data[0];
        $header[5] = 'Couverture';
        $header[6] = 'Qualité du Signal';
        $header[7] = 'Capacité du Réseau';
        $header[8] = 'Transport';
        array_shift($data);
        $data = Controller::toArrayofParameter($data);
        return ['header' => $header, 'data' => $data];
    }

    public static function openCouvertureDataFile(string $file_path)
    {
        $data = Excel::toArray(new CouvertureProperty, public_path($file_path))[0];
        $header = $data[0];
        array_shift($data);
        $result = [];
        foreach ($data as $item) {
            $row = new CouvertureProperty();
            $row->bcfname = $item[0];
            $row->btsname = $item[1];
            $row->sitecode = $item[2];
            $row->value1 = $item[3];
            $row->value2 = $item[4];
            $row->value3 = $item[5];
            $row->value4 = $item[6];
            $row->value5 = $item[7];
            $row->value6 = $item[8];
            $row->value7 = $item[9];
            $row->detected = $item[10];
            $result[] = $row;
        }
        return ['header' => $header, 'data' => $result];
    }

    public static function openQualiteDataFile(string $file_path)
    {
        $data = Excel::toArray(new QualiteProperty(), public_path($file_path))[0];
        $header = $data[0];
        array_shift($data);
        $result = [];
        foreach ($data as $item) {
            $row = new QualiteProperty();
            $row->bcfname = $item[0];
            $row->btsname = $item[1];
            $row->sitecode = $item[2];
            $row->value1 = $item[3];
            $row->value2 = $item[4];
            $row->value3 = $item[5];
            $row->value4 = $item[6];
            $row->value5 = $item[7];
            $row->value6 = $item[8];
            $row->detected = $item[9];
            $result[] = $row;
        }
        return ['header' => $header, 'data' => $result];
    }

    public static function openCapaciteDataFile(string $file_path)
    {
        $data = Excel::toArray(new CapaciteProperty(), public_path($file_path))[0];
        $header = $data[0];
        array_shift($data);
        $result = [];
        foreach ($data as $item) {
            $row = new CapaciteProperty();
            $row->bcfname = $item[0];
            $row->btsname = $item[1];
            $row->sitecode = $item[2];
            $row->value1 = $item[3];
            $row->value2 = $item[4];
            $row->value3 = $item[5];
            $row->value4 = $item[6];
            $row->detected = $item[7];
            $result[] = $row;
        }
        return ['header' => $header, 'data' => $result];
    }

    public static function openTransportDataFile(string $file_path)
    {
        $data = Excel::toArray(new TransportProperty(), public_path($file_path))[0];
        $header = $data[0];
        array_shift($data);
        $result = [];
        foreach ($data as $item) {
            $row = new TransportProperty();
            $row->bcfname = $item[0];
            $row->btsname = $item[1];
            $row->sitecode = $item[2];
            $row->detected = $item[3];
            $result[] = $row;
        }
        return ['header' => $header, 'data' => $result];
    }

    public static function toArrayofParameter(array $data)
    {
        $result = [];
        foreach ($data as $item) {
            $row = new DataProperty();
            $row->date = $item[0];
            $row->sitename = $item[1];
            $row->sitecode = $item[2];
            $row->longitude = $item[3];
            $row->latitude = $item[4];
            $row->param1 = $item[5];
            $row->param2 = $item[6];
            $row->param3 = $item[7];
            $row->param4 = $item[8];
            $result[] = $row;
        }
        return $result;
    }

    public static function saveExportFastExcel($data, $path)
    {
        FastExcel()->data(collect($data))->export($path);
    }

    public static function readDataFastExcel($path, $sheet_name = 'MARS_23')
    {
        $collection = (new FastExcel)->importSheets($path)->first();
        try {
            return $collection[$sheet_name];
        } catch (Exception) {
            return $collection;
        }
    }
}


class Breadcrumb
{
    public string $name;
    public array $levels;

    public static function defineBreadcrumb(string $name, array $levels)
    {
        $breadCrumb = new Breadcrumb();
        $breadCrumb->name = $name;
        $breadCrumb->levels = $levels;
        return $breadCrumb;
    }
}


class SiteProperty
{
    public string $site;
    public string $latitude;
    public string $logitude;
}

class SiteProperty_3G implements ToModel, WithChunkReading
{
    public function model(array $row)
    {
        return new SiteProperty([
            'site' => $row[0],
            'latitude' => $row[1],
            'longitude' => $row[2],
        ]);
    }

    public function batchSize(): int
    {
        return 1;
    }

    public function chunkSize(): int
    {
        return 1;
    }
}

class KPISProperty
{
    public string $site;
    public float $param1;
    public float $param2;
    public float $param3;
    public float $param4;
    public float $param5;
    public float $param6;
    public float $param7;
    public float $param8;
}

class DataProperty
{
    public string $date;
    public string $sitename;
    public string $sitecode;
    public string $latitude;
    public string $longitude;
    public string|null $param1;
    public string|null $param2;
    public string|null $param3;
    public string|null $param4;
}

class CouvertureProperty implements FromCollection, WithHeadings
{
    public string $bcfname;
    public string $btsname;
    public string $sitecode;
    public string|null $value1;
    public string|null $value2;
    public string|null $value3;
    public string|null $value4;
    public string|null $value5;
    public string|null $value6;
    public string|null $value7;
    public string|null $detected;

    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }
    public function headings(): array
    {
        return [
            'BCF Name',
            'BTS Name',
            'Site Code',
            'Couverture',
            'AV UL Signal',
            'TAV [0,1[',
            'TAV [1,2[',
            'TAV [2,5[',
            'TAV [5,8[',
            'TAV [8,10[',
            'TAV [10,...[',
        ];
    }
}

class CapaciteProperty implements FromCollection, WithHeadings
{
    public string $bcfname;
    public string $btsname;
    public string $sitecode;
    public string|null $value1;
    public string|null $value2;
    public string|null $value3;
    public string|null $value4;
    public string|null $detected;

    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }
    public function headings(): array
    {
        return [
            'BCF Name',
            'BTS Name',
            'Site Code',
            'Capacité du Réseau',
            'ORA_2G_HR_RATIO',
            'DHR Usage',
            'SDCCH BR',
            'TCH BR',
        ];
    }
}

class TransportProperty implements FromCollection, WithHeadings
{
    public string $bcfname;
    public string $btsname;
    public string $sitecode;
    public string|null $detected;

    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }
    public function headings(): array
    {
        return [
            'BCF Name',
            'BTS Name',
            'Site Code',
            'Transport',
        ];
    }
}

class QualiteProperty implements FromCollection, WithHeadings
{
    public string $bcfname;
    public string $btsname;
    public string $sitecode;
    public string|null $value1;
    public string|null $value2;
    public string|null $value3;
    public string|null $value4;
    public string|null $value5;
    public string|null $value6;
    public string|null $detected;

    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }
    public function headings(): array
    {
        return [
            'BCF Name',
            'BTS Name',
            'Site Code',
            'Qualité du Signal',
            'DL CumR',
            'Infer Band 1',
            'Infer Band 2',
            'Infer Band 3',
            'Infer Band 4',
            'Infer Band 5',
        ];
    }
}

class DataExport implements FromCollection, WithHeadings
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Period Start Time',
            'Site Name',
            'Site Code',
            'longitude',
            'latitude',
            'param1',
            'param2',
            'param3',
            'param4',
        ];
    }
}

class ParamIntervals
{
    public string $ID;
    public string $NAME;
    public string $LABEL;
    public string $MESURE;
    public float $VERYGOOD_MIN;
    public float $VERYGOOD_MAX;
    public float $GOOD_MAX;
    public float $GOOD_MIN;
    public float $AVERAGE_MAX;
    public float $AVERAGE_MIN;
    public float $BAD_MAX;
    public float $BAD_MIN;

    public static function allIntervals(int $generation = 2)
    {
        // First Problem Intervals
        $param1 = new ParamIntervals();
        $param1->ID = 'pb1';
        $param1->NAME = 'Couverture';
        $param1->LABEL = ($generation == 2) ? 'RxLev_Samples_Below99dBm' : 'CPICH RSCP bad perc';
        $param1->MESURE = '%';
        $param1->VERYGOOD_MIN = ($generation == 2) ? 0 : 0;
        $param1->VERYGOOD_MAX = ($generation == 2) ? 0.04 : 0;
        $param1->GOOD_MIN = ($generation == 2) ? 0.04 : 0;
        $param1->GOOD_MAX = ($generation == 2) ? 0.1 : 0;
        $param1->AVERAGE_MIN = ($generation == 2) ? 0.1 : 0;
        $param1->AVERAGE_MAX = ($generation == 2) ? 0.5 : 0;
        $param1->BAD_MIN = ($generation == 2) ? 0.5 : 0;
        $param1->BAD_MAX = ($generation == 2) ? 100 : 0;

        // Second Problem Intervals
        $param2 = new ParamIntervals();
        $param2->ID = 'pb2';
        $param2->NAME = 'Qualité du Signal';
        $param2->LABEL = ($generation == 2) ?  'DL cumulative quality ratio in class 5' : 'Perc of Bad EcNo';
        $param2->MESURE = '%';
        $param2->VERYGOOD_MIN = ($generation == 2) ? 97 : 0;
        $param2->VERYGOOD_MAX = ($generation == 2) ? 100 : 0;
        $param2->GOOD_MIN = ($generation == 2) ? 95 : 0;
        $param2->GOOD_MAX = ($generation == 2) ? 97 : 0;
        $param2->AVERAGE_MIN = ($generation == 2) ? 90 : 0;
        $param2->AVERAGE_MAX = ($generation == 2) ? 95 : 0;
        $param2->BAD_MIN = ($generation == 2) ? 0 : 0;
        $param2->BAD_MAX = 90;

        // Third Problem Intervals
        $param3 = new ParamIntervals();
        $param3->ID = 'pb3';
        $param3->NAME = 'Capacité du Réseau';
        $param3->LABEL = ($generation == 2) ? 'ORA_2G_HR_RATIO' : 'Radio Congestion';
        $param3->MESURE = '%';
        $param3->VERYGOOD_MIN = ($generation == 2) ? 0 : 0;
        $param3->VERYGOOD_MAX = ($generation == 2) ? 50 : 0;
        $param3->GOOD_MIN = ($generation == 2) ? 50 : 0;
        $param3->GOOD_MAX = ($generation == 2) ? 65 : 0;
        $param3->AVERAGE_MIN = ($generation == 2) ? 65 : 0;
        $param3->AVERAGE_MAX = ($generation == 2) ? 80 : 0;
        $param3->BAD_MIN = ($generation == 2) ? 80 : 0;
        $param3->BAD_MAX = ($generation == 2) ? 100 : 0;

        // Fourth Problem Intervals
        $param4 = new ParamIntervals();
        $param4->ID = 'pb4';
        $param4->NAME = 'Transport';
        $param4->LABEL = ($generation == 2) ? 'DLPck LossR UPlaneRTP CS' : 'HS-DSCH CREDIT REDUCTIONS DUE TO FRAME LOSS';
        $param4->MESURE = ($generation == 2) ? '%' : '';
        $param4->VERYGOOD_MIN = ($generation == 2) ? 0 : 0;
        $param4->VERYGOOD_MAX = ($generation == 2) ? 0.5 : 0;
        $param4->GOOD_MIN = ($generation == 2) ? 0.5 : 0;
        $param4->GOOD_MAX = ($generation == 2) ? 0.7 : 0;
        $param4->AVERAGE_MIN = ($generation == 2) ? 0.7 : 0;
        $param4->AVERAGE_MAX = ($generation == 2) ? 1.5 : 0;
        $param4->BAD_MIN = ($generation == 2) ? 1.5 : 0;
        $param4->BAD_MAX = ($generation == 2) ? 100 : 0;

        $intervals = [
            'PROBLEM1' => $param1,
            'PROBLEM2' => $param2,
            'PROBLEM3' => $param3,
            'PROBLEM4' => $param4
        ];
        return $intervals;
    }

    public static function stat_entities()
    {
        return [
            [
                'label' => 'Très Bonne Qualité',
                'short' => 'vgq',
                'param' => 'very_good',
                'class' => 'success',
                'color' => 'green'
            ],
            [
                'label' => 'Bonne Qualité',
                'short' => 'gq',
                'param' => 'good',
                'class' => 'info',
                'color' => 'blue'
            ],
            [
                'label' => 'Moyenne Qualité',
                'short' => 'avg',
                'param' => 'average',
                'class' => 'warning',
                'color' => 'orange'
            ],
            [
                'label' => 'Mauvaise Qualité',
                'short' => 'bad',
                'param' => 'bad',
                'class' => 'danger',
                'color' => 'red'
            ],
        ];
    }
}
