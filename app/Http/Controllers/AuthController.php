<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if ($request->isMethod('GET')) {
            return view('auth.login');
        } else {
            $login = $request->input('login');
            $password = $request->input('password');

            if (Auth::attempt(['username' => $login, 'password' => $password])) {
                return redirect()->route('user.dashboard');
            };
            Session::flash('message', 'Incorrect credentials !');
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login');
    }
}