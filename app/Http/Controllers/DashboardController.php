<?php

namespace App\Http\Controllers;

use App\Models\Analysis;
use App\Models\Ressource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $all_imports = Ressource::where(['type' => 'IMPORTED'])->get()->count();
        $my_imports = Ressource::where(['type' => 'IMPORTED', 'user_id' => Auth::user()->id])->get()->count();

        $all_processed = Ressource::where(['type' => 'PROCESSED'])->get()->count();
        $my_processed = Ressource::where(['type' => 'PROCESSED', 'user_id' => Auth::user()->id])->get()->count();

        $all_generation = Ressource::all()->count() / 2;
        $generation_2G = Ressource::where(['generation' => '2G', 'user_id' => Auth::user()->id])->get()->count() / 2;
        $generation_3G = Ressource::where(['generation' => '3G', 'user_id' => Auth::user()->id])->get()->count() / 2;

        $statistics = [
            'imports' => [$my_imports, $all_imports],
            'processed' => [$my_processed, $all_processed],
            'generation' => [$all_generation, $generation_2G, $generation_3G],
            'problems' => [
                [
                    'name' => 'Qualité du Signal',
                    'mine' => Analysis::where(['param_name' => 'pb1', 'user_id' => Auth::user()->id])->get()->count(),
                    'total' => Analysis::where(['param_name' => 'pb1'])->get()->count(),
                ],
                [
                    'name' => 'Transport',
                    'mine' => Analysis::where(['param_name' => 'pb2', 'user_id' => Auth::user()->id])->get()->count(),
                    'total' => Analysis::where(['param_name' => 'pb2'])->get()->count(),
                ],
                [
                    'name' => 'Couverture',
                    'mine' => Analysis::where(['param_name' => 'pb3', 'user_id' => Auth::user()->id])->get()->count(),
                    'total' => Analysis::where(['param_name' => 'pb3'])->get()->count(),
                ],
                [
                    'name' => 'Capacité du Réseau',
                    'mine' => Analysis::where(['param_name' => 'pb4', 'user_id' => Auth::user()->id])->get()->count(),
                    'total' => Analysis::where(['param_name' => 'pb4'])->get()->count(),
                ]
            ],
        ];

        $breadCrumb = Breadcrumb::defineBreadcrumb('Accueil', ['Tableau de Bord']);
        return view('dashboard', compact('breadCrumb', 'statistics'));
    }
}