<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Termwind\Components\BreakLine;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $breadCrumb = Breadcrumb::defineBreadcrumb('Utilisateurs', ['List']);
        $users = User::all();
        return view('user.list', compact('users', 'breadCrumb'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $breadCrumb = Breadcrumb::defineBreadcrumb('Utilisateurs', ['Nouveau']);
        return view('user.create', compact('breadCrumb'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $data = $request->validated();
        $avatar = $this::storeImage($request->file('avatar'), Auth::user()->ref);
        $query = [
            'ref' => $this::randString(10),
            'username' => $data['username'],
            'firstname' => $data['firstname'] ?? null,
            'lastname' => $data['lastname'] ?? null,
            'email' => $data['email'] ?? null,
            'phone' => $data['phone'] ?? null,
            'avatar' => $avatar,
            'role' => $data['role'],
            'password' => bcrypt($data['password']),
        ];
        User::create($query);
        return redirect()->route('user.account.list');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}