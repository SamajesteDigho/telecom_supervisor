<?php

namespace App\Http\Controllers;

use App\Models\Ressource;
use Carbon\Carbon;
use DateTime;
use Exception;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Throwable;

class ImportController extends Controller
{
    public function index(Request $request)
    {
        $site_2G = false;
        $site_3G = false;
        if (File::exists(public_path($this::SITE_2G_FILE))) {
            $site_2G = true;
        }
        if (File::exists(public_path($this::SITE_3G_FILE))) {
            $site_3G = true;
        }
        $breadCrumb = Breadcrumb::defineBreadcrumb('Import', ['Nouvel']);
        return view('import.index', compact('breadCrumb', 'site_2G', 'site_3G'));
    }
    public function ftp_upload(Request $request)
    {
        $ftp_server = $request->input('ftp_server');
        $ftp_port = $request->input('port') ?? 21;
        $ftp_username = $request->input('ftp_username');
        $ftp_password = $request->input('ftp_password') ?? '';
        $ftp_server_file = $request->input('ftp_server_file');
        $ftp_server_file_extension = $request->input('ftp_server_file_extension');
        $generation = $request->input('generation') == '2G' ? 2 : 3;
        $selected_date = $request->input('selected_date');
        $kpis_filename = $generation . 'G_' . Auth::user()->username . '_' . Carbon::now()->timestamp . '.' . $ftp_server_file_extension;
        $file_path = $this::KPIS_FILE_DIRECTORY . '/' . $kpis_filename;

        try {
            $ftp_conn = null;
            $ftp_conn = ftp_connect($ftp_server, $ftp_port) or die('could not connect to ' . $ftp_server);
            $login = ftp_login($ftp_conn, $ftp_username, $ftp_password);

            if (ftp_get($ftp_conn, $file_path, $ftp_server_file, FTP_ASCII, 0)) {
                Ressource::create([
                    'filename' => $kpis_filename,
                    'user_id' => Auth::user()->id,
                    'type' => 'IMPORTED',
                    'generation' => $generation . 'G'
                ]);

                if ($ftp_server_file_extension != 'xlsx' || $ftp_server_file_extension != 'xls') {
                    Session::flash('message', 'Extension innaproprié. Veuillez choisir un ficher excel.');
                    Session::flash('alert-class', 'danger');
                    return back()->withInput();
                }

                $filename = $this->combineFiles($kpis_filename, $generation, $selected_date);
                return redirect()->route('user.import.display', ['filename' => explode('.', $filename)[0]]);
            } else {
                Session::flash('message', 'Impossible de télécharger le fichier ou fichier introuvable');
                Session::flash('alert-class', 'danger');
                return back()->withInput();
            }
        } catch (Exception $e) {
            Session::flash('message', $e->getMessage());
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        } finally {
            if ($ftp_conn) {
                ftp_close($ftp_conn);
            }
        }
    }

    public function upload(Request $request)
    {
        $generation = $request->input('generation') == '2G' ? 2 : 3;
        $selected_date = $request->input('selected_date') ?? null;

        set_time_limit(300);
        if (!$request->hasFile('kpis')) {
            Session::flash('message', 'The KPIS file is required. Please select one.');
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        }
        if ($request->hasFile('site')) {
            $this::saveSiteTemplate($request->file('site'), $generation);
        }

        $kpis = $this::saveFile($request->file('kpis'), Auth::user()->username, $generation . 'G', 'kpis');
        Ressource::create([
            'filename' => $kpis,
            'user_id' => Auth::user()->id,
            'type' => 'IMPORTED',
            'generation' => $generation . 'G'
        ]);

        $filename = $this->combineFiles($kpis, $generation, $selected_date);
        return redirect()->route('user.import.display', ['filename' => explode('.', $filename)[0]]);
    }

    public function display(Request $request, string $filename)
    {
        try {
            $file_path = $this::PROCESSED_FILE_DIRECTORY . $filename . '.xlsx';
            $res = $this::openDataFile($file_path);
            $header = $res['header'];
            $data = $res['data'];
            $breadCrumb = Breadcrumb::defineBreadcrumb('Import', ['Affichage', $filename]);
            return view('import.display', compact('breadCrumb', 'filename', 'data', 'header'));
        } catch (Throwable $e) {
            Session::flash('message', $e->getMessage());
            Session::flash('alert-class', 'danger');
            return back();
        }
    }



    /**
     * ==================================================================
     * Here the util function helping
     * ==================================================================
     */
    private function combineFiles(string $kpis_path, int $generation, $selected_date = null)
    {
        if ($generation == 2) {
            $site_rows = $this::readDataFastExcel(public_path($this::SITE_2G_FILE), 'MARS_23');
        } else {
            $site_rows = $this::readDataFastExcel(public_path($this::SITE_3G_FILE), 'MARS_23');
        }
        // return $site_rows;
        // @todo Open the files
        //$kpis_rows = Excel::toArray(new KPISProperty, public_path($this::KPIS_FILE_DIRECTORY . $kpis_path))[0];
        $kpis_rows = $this::readDataFastExcel(public_path($this::KPIS_FILE_DIRECTORY . $kpis_path), 'Data');

        // Site file attributes
        $sf_sitename_idx = 'Nom du Site'; //array_search('Cellule', $site_header);
        $sf_sitecode_idx = 'Code site'; //array_search('Code site', $site_header);
        $sf_longitude_idx = 'Longitude'; //array_search('Longitude', $site_header);
        $sf_latitude_idx = 'Latitude'; //array_search('Latitude', $site_header);
        $kf_kpisbcf_idx = ($generation == 2) ? 'BCF name' : 'WBTS name'; //array_search('BCF name', $kpis_header);
        $kf_kpisdate_idx = 'Period start time'; //array_search('Period start time', $kpis_header);
        $kf_kpisparam1_idx = ($generation == 2) ? 'RxLev_Samples_Below99dBm' : 'CPICH RSCP bad perc';
        $kf_kpisparam2_idx = ($generation == 2) ? 'DL cumulative quality ratio in class 5' : 'Perc of Bad EcNo';
        $kf_kpisparam3_idx = ($generation == 2) ? 'ORA_2G_HR_RATIO' : 'Radio Congestion';
        $kf_kpisparam4_idx = ($generation == 2) ? 'DLPck LossR UPlaneRTP CS' : 'HS-DSCH CREDIT REDUCTIONS DUE TO FRAME LOSS';

        // Remove the headers

        // array_shift($site_rows);
        array_shift($kpis_rows);
        $combined = [];

        // @todo Process the files and combine
        // $datetime = new DateTime();
        foreach ($kpis_rows as $row) {
            $code = substr($row[$kf_kpisbcf_idx], -7);
            $property = $this->searchTextFromExcel($site_rows, $code, $sf_sitecode_idx);
            if ($property != null) {
                $datetime = new DateTime("@{$row[$kf_kpisdate_idx]->getTimeStamp()}");
                $data = new DataProperty();
                $data->date = $datetime->format('Y-m-d');
                $data->sitename = $property[$sf_sitename_idx];
                $data->sitecode = $property[$sf_sitecode_idx];
                $data->longitude = $property[$sf_longitude_idx];
                $data->latitude = $property[$sf_latitude_idx];
                $data->param1 = $row[$kf_kpisparam1_idx] ?? 0;
                $data->param2 = $row[$kf_kpisparam2_idx] ?? 0;
                $data->param3 = $row[$kf_kpisparam3_idx] ?? 0;
                $data->param4 = $row[$kf_kpisparam4_idx] ?? 0;
                if ($selected_date == null || ($selected_date == $data->date)) {
                    $combined[] = (object) $data;
                }
            }
        }

        // @todo Convert into csv
        $export = new DataExport($combined);

        $filename = $kpis_path;
        $fh = fopen(public_path() . '/' . $this::PROCESSED_FILE_DIRECTORY . $filename, 'w+');
        fclose($fh);
        $path = $this::PROCESSED_FILE_DIRECTORY . $filename;
        try {
            // $this::saveExportFastExcel($export, public_path($path));
            Excel::store($export, $path, 'public');
        } catch (Throwable $e) {
            Log::error('Error storing Excel file: ' . $e->getMessage());
            return 'Error storing Excel file. Chenck the logs.';
        }
        Ressource::create([
            'filename' => $filename,
            'user_id' => Auth::user()->id,
            'type' => 'PROCESSED',
            'generation' => $generation . 'G',
        ]);
        return $filename;
    }

    private function searchTextFromExcel($rows, string $sitecode, string $idx)
    {
        foreach ($rows as $row) {
            if (trim($row[$idx]) == trim($sitecode)) {
                return $row;
            }
        }
        return null;
    }
}