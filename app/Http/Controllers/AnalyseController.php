<?php

namespace App\Http\Controllers;

use App\Models\Analysis;
use App\Models\Ressource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class AnalyseController extends Controller
{
    public function store_analysis(Request $request)
    {
        try {
            $query = [
                'filename' => $request->get('filename') ?? 'Not defined',
                'user_id' => $request->get('user_id') ?? 1,
                'param_name' => $request->get('param_name') ?? 'Not defined',
                'generation' => $request->get('generation') ?? '2G',
                'duration' => $request->get('duration') ?? 0,
            ];
            Analysis::create($query);

            return response()->json([
                'success' => true,
                'data' => $query,
                'message' => "Successfull store analysis",
            ], 200);
        } catch (Throwable $e) {
            return response()->json([
                'success' => true,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function index(Request $request)
    {
        $files = Ressource::where(['type' => 'PROCESSED'])->get();
        $breadCrumb = Breadcrumb::defineBreadcrumb('Detection', ['Fichiers']);
        return view('analyse.index', compact('breadCrumb', 'files'));
    }

    public function store_detection_file(Request $request): JsonResponse
    {
        if (!$request->hasFile('file')) {
            return response()->json([
                'success' => false,
                'message' => 'No file could be found : ' . $request->file()
            ], 400);
        }

        $filename = $this::saveDegradedFile($request->file('file'), $request->input('filename'), 'final');
        return response()->json([
            'success' => true,
            'message' => 'Successfully stored the file : ' . $filename
        ], 200);
    }

    public function analyse_file(Request $request, $filename)
    {
        // Collect Quality Parameter
        $intervals = ParamIntervals::allIntervals($filename[0]);
        $stat_entities = ParamIntervals::stat_entities();
        $generation = $filename[0] . $filename[1];
        try {
            $file_path = $this::PROCESSED_FILE_DIRECTORY . $filename . '.xlsx';
            $res = $this::openDataFile($file_path);
            $header = $res['header'];
            $data = $res['data'];
        } catch (Exception $e) {
            $data = [];
            $header = [];
        }
        $breadCrumb = Breadcrumb::defineBreadcrumb('Detection', ['Fichiers', $generation, $filename]);
        return view('analyse.analyse', compact('breadCrumb', 'file_path', 'generation', 'data', 'header', 'intervals', 'stat_entities'));
    }

    public function advanced_index(Request $request)
    {
        return view('advanced_analysis.import');
    }

    public function advanced_import(Request $request)
    {
        $generation = $request->input('generation') == '2G' ? 2 : 3;
        // Store the files
        $kpis_file = $this::saveFileFromName($request->file('kpis'), $generation . 'G_file_parameters.', 'final');

        $couverture = $generation . 'G_Couverture.xlsx';
        $capacite = $generation . 'G_Capacite.xlsx';
        $qualite = $generation . 'G_Qualite.xlsx';
        $transport = $generation . 'G_Transport.xlsx';

        if (is_file(public_path($this::TREATEMENT_FILE_DIRECTORY . $couverture))) {
            $this->couverture_extraction($kpis_file, $couverture);
        }
        if (is_file(public_path($this::TREATEMENT_FILE_DIRECTORY . $capacite))) {
            $this->capacite_extraction($kpis_file, $capacite);
        }
        if (is_file(public_path($this::TREATEMENT_FILE_DIRECTORY . $qualite))) {
            $this->qualite_extraction($kpis_file, $qualite);
        }
        if (is_file(public_path($this::TREATEMENT_FILE_DIRECTORY . $transport))) {
            $this->transport_extraction($kpis_file, $transport);
        }

        return redirect()->route('user.advanced_analysis.treatement');
    }

    public function advanced_treatement(Request $request)
    {
        $file_path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/2G_Couverture.xlsx';
        $couverture = [];
        $couverture_header = [];
        if (is_file(public_path($file_path))) {
            $res = $this::openCouvertureDataFile($file_path);
            $couverture = $res['data'];
            $couverture_header = $res['header'];
        }

        $file_path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/2G_Capacite.xlsx';
        $capacite = [];
        $capacite_header = [];
        if (is_file(public_path($file_path))) {
            $res = $this::openCapaciteDataFile($file_path);
            $capacite = $res['data'];
            $capacite_header = $res['header'];
        }

        $file_path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/2G_Qualite.xlsx';
        $qualite = [];
        $qualite_header = [];
        if (is_file(public_path($file_path))) {
            $res = $this::openQualiteDataFile($file_path);
            $qualite = $res['data'];
            $qualite_header = $res['header'];
        }


        $file_path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/2G_Transport.xlsx';
        $transport = [];
        $transport_header = [];
        if (is_file(public_path($file_path))) {
            $res = $this::openTransportDataFile($file_path);
            $transport = $res['data'];
            $transport_header = $res['header'];
        }

        $breadCrumb = Breadcrumb::defineBreadcrumb('Analyse', ['ADvanced Treatment', '2G']);
        return view('advanced_analysis.treatement', compact('couverture', 'capacite', 'qualite', 'transport', 'couverture_header', 'capacite_header', 'qualite_header', 'transport_header'));
    }


    /**
     * ======================================================================================
     */

    private function couverture_extraction(string $kpis_path, string $cou_path)
    {
        $kpis_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $kpis_path), 'Data');
        $real_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $cou_path), 'myWorkSheet');



        // array_shift($site_rows);
        array_shift($kpis_rows);
        array_shift($kpis_rows);

        // Site file attributes
        $kpi_bcf_name = 'BCF name';
        $kpi_bts_name = 'BTS name';
        $kpi_tav_1 = 'Timing_Advance_value_[0..1[';
        $kpi_tav_2 = 'Timing_Advance_value_[1..2[';
        $kpi_tav_3 = 'Timing_Advance_value_[2..5[';
        $kpi_tav_4 = 'Timing_Advance_value_[5..8[';
        $kpi_tav_5 = 'Timing_Advance_value_[8..10[';
        $kpi_tav_6 = 'Timing_Advance_value_[10..-[';
        $kpi_tav_7 = 'Average UL signal strength';
        $real_site_code = 'sitecode';
        $real_param_value = 'param_name';

        $data_couverture = [];

        foreach ($real_rows as $row) {
            $code = $row[$real_site_code];
            $properties = $this::searchMultiCorresponding($kpis_rows, $code, $kpi_bcf_name);
            foreach ($properties as $property) {
                $data = new CouvertureProperty();
                $data->bcfname = $property[$kpi_bcf_name];
                $data->btsname = $property[$kpi_bts_name];
                $data->sitecode = $code;
                $data->value1 = round(floatval($property[$kpi_tav_1] ?? '0'), 2);
                $data->value2 = round(floatval($property[$kpi_tav_2] ?? '0'), 2);
                $data->value3 = round(floatval($property[$kpi_tav_3] ?? '0'), 2);
                $data->value4 = round(floatval($property[$kpi_tav_4] ?? '0'), 2);
                $data->value5 = round(floatval($property[$kpi_tav_5] ?? '0'), 2);
                $data->value6 = round(floatval($property[$kpi_tav_6] ?? '0'), 2);
                $data->value7 = round(floatval($property[$kpi_tav_7] ?? '0'), 2);
                $data->detected = round(floatval($row[$real_param_value] ?? '0'), 2);

                $data_couverture[] = $data;
            }
        }
        // @todo Convert into csv
        $export = new CouvertureProperty($data_couverture);

        $filename = $cou_path;
        $fh = fopen(public_path($this::TREATEMENT_FILE_DIRECTORY) . 'extracted/' . $filename, 'w+');
        fclose($fh);
        $path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/' . $filename;
        try {
            Excel::store($export, $path, 'public');
        } catch (Throwable $e) {
            Log::error('Error storing Excel file: ' . $e->getMessage());
            return 'Error storing Excel file. Chenck the logs.';
        }
        return $filename;
    }

    private function qualite_extraction(string $kpis_path, string $cap_path)
    {
        $kpis_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $kpis_path), 'Data');
        $real_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $cap_path), 'myWorkSheet');

        // array_shift($site_rows);
        array_shift($kpis_rows);
        array_shift($kpis_rows);

        // Site file attributes
        $kpi_bcf_name = 'BCF name';
        $kpi_bts_name = 'BTS name';
        $kpi_tav_1 = 'DL cumulative quality ratio in class 5';
        $kpi_tav_2 = 'Interference_Band1';
        $kpi_tav_3 = 'Interference_Band3';
        $kpi_tav_4 = 'Interference_Band2';
        $kpi_tav_5 = 'Interference_Band4';
        $kpi_tav_6 = 'Interference_Band5';
        $real_site_code = 'sitecode';
        $real_param_value = 'param_name';

        $data_qualite = [];

        foreach ($real_rows as $row) {
            $code = $row[$real_site_code];
            $properties = $this::searchMultiCorresponding($kpis_rows, $code, $kpi_bcf_name);
            foreach ($properties as $property) {
                $data = new QualiteProperty();
                $data->bcfname = $property[$kpi_bcf_name];
                $data->btsname = $property[$kpi_bts_name];
                $data->sitecode = $code;
                $data->value1 = round(floatval($property[$kpi_tav_1] ?? 0), 2);
                $data->value2 = round(floatval($property[$kpi_tav_2] ?? 0), 2);
                $data->value3 = round(floatval($property[$kpi_tav_3] ?? 0), 2);
                $data->value4 = round(floatval($property[$kpi_tav_4] ?? 0), 2);
                $data->value5 = round(floatval($property[$kpi_tav_5] ?? 0), 2);
                $data->value6 = round(floatval($property[$kpi_tav_6] ?? 0), 2);
                $data->detected = round(floatval($row[$real_param_value] ?? 0), 2);

                $data_qualite[] = $data;
            }
        }
        // @todo Convert into csv
        $export = new QualiteProperty($data_qualite);

        $filename = $cap_path;
        $fh = fopen(public_path($this::TREATEMENT_FILE_DIRECTORY) . 'extracted/' . $filename, 'w+');
        fclose($fh);
        $path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/' . $filename;
        try {
            Excel::store($export, $path, 'public');
        } catch (Throwable $e) {
            Log::error('Error storing Excel file: ' . $e->getMessage());
            return 'Error storing Excel file. Chenck the logs.';
        }
        return $filename;
    }

    private function transport_extraction(string $kpis_path, string $trans_path)
    {
        $kpis_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $kpis_path), 'Data');
        $real_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $trans_path), 'myWorkSheet');

        // array_shift($site_rows);
        array_shift($kpis_rows);
        array_shift($kpis_rows);

        // Site file attributes
        $kpi_bcf_name = 'BCF name';
        $kpi_bts_name = 'BTS name';
        $real_site_code = 'sitecode';
        $real_param_value = 'param_name';

        $data_transport = [];

        foreach ($real_rows as $row) {
            $code = $row[$real_site_code];
            $property = $this::searchCorresponding($kpis_rows, $code, $kpi_bcf_name);
            if ($property != null) {
                $data = new TransportProperty();
                $data->bcfname = $property[$kpi_bcf_name];
                $data->btsname = $property[$kpi_bts_name];
                $data->sitecode = $code;
                $data->detected = round(floatval($row[$real_param_value] ?? 0), 2);

                $data_transport[] = $data;
            }
        }
        // @todo Convert into csv
        $export = new TransportProperty($data_transport);

        $filename = $trans_path;
        $fh = fopen(public_path($this::TREATEMENT_FILE_DIRECTORY) . 'extracted/' . $filename, 'w+');
        fclose($fh);
        $path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/' . $filename;
        try {
            Excel::store($export, $path, 'public');
        } catch (Throwable $e) {
            Log::error('Error storing Excel file: ' . $e->getMessage());
            return 'Error storing Excel file. Chenck the logs.';
        }
        return $filename;
    }

    private function capacite_extraction(string $kpis_path, string $qua_path)
    {
        $kpis_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $kpis_path), 'Data');
        $real_rows = $this::readDataFastExcel(public_path($this::TREATEMENT_FILE_DIRECTORY . $qua_path), 'myWorkSheet');

        // Site file attributes
        $kpi_bcf_name = 'BCF name';
        $kpi_bts_name = 'BTS name';
        $kpi_tav_1 = 'ORA_2G_HR_RATIO';
        $kpi_tav_2 = 'DHR Usage';
        $kpi_tav_3 = 'SDCCH_Blocking_Rate(%)';
        $kpi_tav_4 = 'TCH_Blocking_Rate (%)';
        $real_site_code = 'sitecode';
        $real_param_value = 'param_name';

        $data_capacite = [];

        foreach ($real_rows as $row) {
            $code = $row[$real_site_code];
            $properties = $this::searchMultiCorresponding($kpis_rows, $code, $kpi_bcf_name);
            foreach ($properties as $property) {
                $data = new CapaciteProperty();
                $data->bcfname = $property[$kpi_bcf_name];
                $data->btsname = $property[$kpi_bts_name];
                $data->sitecode = $code;
                $data->value1 = round(floatval($property[$kpi_tav_1] ?? 0), 2);
                $data->value2 = round(floatval($property[$kpi_tav_2] ?? 0), 2);
                $data->value3 = round(floatval($property[$kpi_tav_3] ?? 0), 2);
                $data->value4 = round(floatval($property[$kpi_tav_4] ?? 0), 2);
                $data->detected = round(floatval($row[$real_param_value] ?? 0), 2);

                $data_capacite[] = $data;
            }
        }
        // @todo Convert into csv
        $export = new CapaciteProperty($data_capacite);

        $filename = $qua_path;
        $fh = fopen(public_path($this::TREATEMENT_FILE_DIRECTORY) . 'extracted/' . $filename, 'w+');
        fclose($fh);
        $path = $this::TREATEMENT_FILE_DIRECTORY . 'extracted/' . $filename;
        try {
            Excel::store($export, $path, 'public');
        } catch (Throwable $e) {
            Log::error('Error storing Excel file: ' . $e->getMessage());
            return 'Error storing Excel file. Chenck the logs.';
        }
        return $filename;
    }

    static private function searchCorresponding($rows, string $search_value, string $column_name)
    {
        foreach ($rows as $row) {
            $code = substr($row[$column_name], -7);
            if (trim($code) == trim($search_value)) {
                return $row;
            }
        }
        return null;
    }

    static private function searchMultiCorresponding($rows, string $search_value, string $column_name)
    {
        $data = [];
        foreach ($rows as $row) {
            $code = substr($row[$column_name], -7);
            if (trim($code) == trim($search_value)) {
                $data[] = $row;
            }
        }
        return $data;
    }
}
